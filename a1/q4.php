<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $x = 8;
    echo "Value is now $x </br>";
    echo "Add 2. Value is now " . ($x = $x + 2) . "</br>";
    echo "Subtract 4. Value is now " . ($x = $x - 4) . "</br>";
    echo "Multiply by 5. Value is now " . ($x = $x * 5) . "</br>";
    echo "Divide by 3. Value is now " . ($x = $x / 3) . "</br>";
    echo "Increment value by 1. Value is now " . (++$x) . "</br>";
    echo "Decrement value by 1. Value is now " . (--$x) . "</br>";
    ?>
</body>

</html>
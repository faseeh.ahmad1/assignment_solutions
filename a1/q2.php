<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2</title>
</head>
<body>
    <?php 
        //printing static content
        echo "Twinkle, Twinkle little Star.<br/>";
        
        //declaring variables
        $var1 = "Twinkle";
        $var2 = "Star";

        //printing after substituting above values in statment
        echo "$var1, $var1 little $var2.<br/>";

        //changing values of variables
        $var1  = "Hello";
        $var2 = "World";

        //printing again with new values
        echo "$var1, $var1 little $var2.<br/>";

    ?>
</body>
</html>
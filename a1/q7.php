<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>7</title>
</head>

<body>
    <?php
    $x = "whatsit";
    echo "Value is " . gettype($x) . "</br>";
    echo "Value is " . gettype($x = 12.1) . "</br>";
    echo "Value is " . gettype($x = true) . "</br>";
    echo "Value is " . gettype($x = 1) . "</br>";
    echo "Value is " . gettype($x = NULL) . "</br>";
    ?>
</body>

</html>
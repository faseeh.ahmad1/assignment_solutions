<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require 'select.php';
    $browsers = array("None", "Firefox", "Chrome", "IE", "Safari", "Opera", "Other");
    $speeds = array("Unknown", "DSL", "T1", "Cable", "Dialup", "Other");
    $os = array("windows", "linux", "macintosh", "Other");
    ?>
    <div style='display:flex;justify-content:center'>
        <form method='POST' style='max-width:50%;border-radius:10px;padding:50px 20px;background-color:#EEEEEE;display:flex; align-items:flex-start;flex-direction:column;min-width: 40vw;'>

            <!-- Work Access     -->
            <h2 style="margin:0">Work Access</h2>
            <label for='Browser' style='align-self:flex-start;margin-top:20px'>Primary Browser</label>
            <?php
            $selectField = new Select();
            $selectField->set_name("browser");
            $selectField->set_value($browsers);
            $selectField->makeSelect();
            unset($selectField);
            ?>

            <label for='Speed' style='align-self:flex-start;margin-top:20px'>Speed</label>
            <?php
            $selectField = new Select();
            $selectField->set_name("speed");
            $selectField->set_value($speeds);
            $selectField->makeSelect();
            unset($selectField);

            ?>

            <label for='Operating System' style='align-self:flex-start;margin-top:20px'>OS</label>
            <?php
            $selectField = new Select();
            $selectField->set_name("os");
            $selectField->set_value($os);
            $selectField->makeSelect();
            unset($selectField);
            ?>

            <!-- Home Access -->
            <h2 style="margin-bottom:0">Home Access</h2>

            <label for='Browser' style='align-self:flex-start;margin-top:20px'>Primary Browser</label>
            <?php
            $selectField = new Select();
            $selectField->set_name("browser2");
            $selectField->set_value($browsers);
            $selectField->makeSelect();
            unset($selectField);
            ?>

            <label for='Speed' style='align-self:flex-start;margin-top:20px'>Speed</label>
            <?php
            $selectField = new Select();
            $selectField->set_name("speed2");
            $selectField->set_value($speeds);
            $selectField->makeSelect();
            unset($selectField);
            ?>

            <label for='Operating System' style='align-self:flex-start;margin-top:20px'>OS</label>
            <?php
            $selectField = new Select();
            $selectField->set_name("os2");
            $selectField->set_value($os);
            $selectField->makeSelect();
            unset($selectField);
            ?>
            <input type='submit' style='align-self:center;margin-top:10px'>
        </form>
    </div>
    <?php
    // check if form is submitted
    if ($_POST['browser']) {
        echo "<h2>Work Access</h2>";
        echo "<h3>Browser:" . $_POST['browser'] . "</h3>";
        echo "<h3>Speed:" . $_POST['speed'] . "</h3>";
        echo "<h3>OS:" . $_POST['os'] . "</h3>";
        echo "<h2>Home Access</h2>";
        echo "<h3>Browser:" . $_POST['browser2'] . "</h3>";
        echo "<h3>Speed:" . $_POST['speed2'] . "</h3>";
        echo "<h3>OS:" . $_POST['os2'] . "</h3>";
    }
    ?>


</body>

</html>
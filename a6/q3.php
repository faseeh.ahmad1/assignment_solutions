<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // ini_set('display_errors','On');
    // error_reporting(E_ALL);
    require 'select.php';
    $browsers = array("Firefox", "Chrome", "IE", "Safari", "Opera", "Other");
    $speeds = array("Unknown", "DSL", "T1", "Cable", "Dialup", "Other");
    $os = array("windows", "linux", "macintosh", "Other");
    if (!$_POST['browser']) {
        echo "
    <div style='display:flex;justify-content:center'>
        <form method='POST' style='max-width:50%;border-radius:10px;padding:50px 20px;background-color:#EEEEEE;display:flex; align-items:flex-start;flex-direction:column;min-width: 40vw;'>
            
        <label for='name' style='align-self:flex-start'>*Name</label>
            <input type='text' name='name' style='margin-top:20px;width:100%'>
            <label for='username' style='align-self:flex-start;margin-top:20px'>*UserName</label>
            <input type='text' name='username' style='margin-top:20px;width:100%'>
            <label for='email' style='align-self:flex-start;margin-top:20px'>*Email</label>
            <input type='text' name='email' style='margin-top:20px;width:100%'>

            <!-- Work Access     -->
            <h2 style='margin-bottom:0'>Work Access</h2>
            <label for='Browser' style='align-self:flex-start;margin-top:20px'>Primary Browser</label>
            ";

        $selectField = new Select();
        $selectField->set_name('browser');
        $selectField->set_value($browsers);
        $selectField->makeSelect();
        unset($selectField);


        echo "<label for='Speed' style='align-self:flex-start;margin-top:20px'>Speed</label>";
        $selectField = new Select();
        $selectField->set_name('speed');
        $selectField->set_value($speeds);
        $selectField->makeSelect();
        unset($selectField);


        echo "<label for='Operating System' style='align-self:flex-start;margin-top:20px'>OS</label>";
        $selectField = new Select();
        $selectField->set_name('os');
        $selectField->set_value($os);
        $selectField->makeSelect();
        unset($selectField);

        // <!-- Home Access -->
        echo "<h2 style='margin-bottom:0'>Home Access</h2>

            <label for='Browser' style='align-self:flex-start;margin-top:20px'>Primary Browser</label>";

        $selectField = new Select();
        $selectField->set_name('browser2');
        $selectField->set_value($browsers);
        $selectField->makeSelect();
        unset($selectField);


        echo "<label for='Speed' style='align-self:flex-start;margin-top:20px'>Speed</label>";
        $selectField = new Select();
        $selectField->set_name('speed2');
        $selectField->set_value($speeds);
        $selectField->makeSelect();
        unset($selectField);

        echo "<label for='Operating System' style='align-self:flex-start;margin-top:20px'>OS</label>";
        $selectField = new Select();
        $selectField->set_name('os2');
        $selectField->set_value($os);
        $selectField->makeSelect();
        unset($selectField);
        echo "
            <input type='submit' style='align-self:center;margin-top:10px'>
        </form>
    </div>";
    } else {
        if (empty($_POST['name']) || empty($_POST['username']) || empty($_POST['email'])) {
            if (empty($_POST['name'])) {
                echo "<p style='color:red'>Name not provided. Kindly provide name</p>";
            }
            if (empty($_POST['username'])) {
                echo "<p style='color:red'>Username not provided. Kindly provide username</p>";
            }
            if (empty($_POST['email'])) {
                echo "<p style='color:red'>Email not provided. Kindly provide email</p>";
            }   
        } 
        //If email not contains @ symbol
        else if (!empty($_POST['email']) && !strpos($_POST['email'],'@')) {
            echo "<p style='color:red'>Email is not valid.</p>";
        }
        else {
            echo "<h4>Name:" . ($_POST['name'])."</h4>";
            echo "<h4>UserName:" . ($_POST['username']) . "</h4>";
            echo "<h4>Email:" . ($_POST['email']) . "</h4></br>";
            echo "<h2>Work Access</h2>";
            echo "<h4>Browser:" . ($_POST['browser'] === "Select one" ? " not provided" : $_POST['browser']) . "</h4>";
            echo "<h4>Speed:" . ($_POST['speed'] === "Select one" ? " not provided" : $_POST['speed']) . "</h4>";
            echo "<h4>OS:" . ($_POST['os'] === "Select one" ? " not provided" : $_POST['os']) . "</h4>";
            echo "<h2>Home Access</h2>";
            echo "<h4>Browser:" . ($_POST['browser2'] === "Select one" ? " not provided" : $_POST['browser2']) . "</h4>";
            echo "<h4>Speed:" . ($_POST['speed2'] === "Select one" ? " not provided" : $_POST['speed2']) . "</h4>";
            echo "<h4>OS:" . ($_POST['os2'] === "Select one" ? " not provided" : $_POST['os2']) . "</h4>";
        }
        echo "<a href=\"javascript:history.go(-1)\" style='border-radius:5px;text-decoration:none;cursor:pointer;padding:5px;border:1px solid #AAA;max-width:fit-content;margin:30px'>
        GO BACK 
      </a>";
    }
    ?>

</body>

</html>
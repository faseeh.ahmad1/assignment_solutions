<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>6-1</title>
</head>

<body>
    <?php
    $browsers = array("Firefox", "Chrome", "IE", "Safari", "Opera", "Other");
    require 'select.php';
    ?>
    <div style='display:flex;justify-content:center;margin-top:20px'>
    <form method='POST' style='border-radius:10px;padding:50px 20px;background-color:#EEEEEE;display:flex; align-items:flex-start;flex-direction:column;min-width: 40vw;'>
        <label for='name' style='align-self:flex-start'>Name</label>
        <input type='text' name='name' required style='margin-top:20px;width:100%'>
        <label for='username' style='align-self:flex-start;margin-top:20px'>UserName</label>
        <input type='text' name='username' required style='margin-top:20px;width:100%'>
        <label for='email' style='align-self:flex-start;margin-top:20px'>Email</label>
        <input type='text' name='email' required style='margin-top:20px;width:100%'>
        <label for='Browser' style='align-self:flex-start;margin-top:20px'>Select Browser</label>

        <?php
            $selectField = new Select();
            $selectField->set_name("browser");
            $selectField->set_value($browsers);
            $selectField->makeSelect();
        ?>
        <input type='submit' style='align-self:center;margin-top:10px'>
    </form>
    </div>
    
    <?php
        if($_POST['browser']){
            echo "<h3>Name:".$_POST['name']."</h3>";
            echo "<h3>UserName:".$_POST['username']."</h3>";
            echo "<h3>Email:".$_POST['email']."</h3>";
            echo "<h3>Browser:".$_POST['browser']."</h3>";
        }

    ?>
</body>

</html>
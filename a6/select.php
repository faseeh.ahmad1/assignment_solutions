<?php

class Select
    {
        private $name;
        private $value;

        function set_name($name)
        {
            $this->name = $name;
        }

        function get_name()
        {
            return $this->name;
        }

        function set_value($value)
        {
            if (is_array($value))
                $this->value = $value;
            else
                echo "provided value is not array. unable to set value.";
        }

        function get_value()
        {
            return $this->value;
        }

        function createOptions()
        {
            foreach ($this->value as $key) {
                echo "<option value='$key'>$key</option>";
            }
        }

        function makeSelect()
        {
            echo "<select name=$this->name required>";
            array_unshift($this->value,"Select one");
            $this->createOptions();
            echo "</select>";
        }
    }

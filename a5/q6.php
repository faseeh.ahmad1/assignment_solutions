<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5-6</title>
</head>

<body>
    <?php
    // function to create options from provided array
    function createOptions($values)
    {
        foreach ($values as $key => $values) {
            echo "<option name='month' value='$key'>$key</option>";
        }
    }

    //function to render select box with values
    function makeSelect($values)
    {
        //check if provided value is array
        if (is_array($values) == "array") {
            echo "<select name='month'>";
            createOptions($values);
            echo "</select>";
        }
    }
    ?>

    <!-- form for user input -->
    <form method="POST">
        <label for="month">Please select a month </label>
        <?php
        $months = array(
            "January" => 31, "February" => "28 days, if leap year 29", "March" => 31, "April" => 30, "May" => 31,
            "June" => 30, "July" => 31, "August" => 31, "September" => 30, "October" => 31, "November" => 30, "December" => 31
        );
        //using make select to render select box           
        makeSelect($months)
        ?>
        <br />
        <input type="submit" />
    </form>

    <!-- output the result after form submission -->
    <?php
    if ($_POST['month'])
        echo "<p>The month of " . $_POST['month'] . " has " . $months[$_POST['month']] . " days</p>";
    ?>

</body>

</html>
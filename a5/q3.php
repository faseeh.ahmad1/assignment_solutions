<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5-3</title>
</head>

<body>
    <!-- form to take input -->
    <div style="display:flex;align-items:center;flex-direction:column">
        <form method='POST' style='width:50px;border-radius:10px;padding:50px 20px;background-color:#EEEEEE;display:flex; align-items:center;flex-direction:column;min-width: 40vw;'>
            <label for='length' style='align-self:flex-start'>Enter Length</label>
            <input type='number' id='length' name='length' required style='width:100%'>

            <label for='width' style='margin-top:20px;align-self:flex-start'>Enter Width</label>
            <input type='number' id='width' name='width' required style='width:100%'>

            <input type='submit' style='margin-top:20px; width:50%''>
    </form>
    <br/>

    <!-- function decleration and calling on form submit -->
    <?php
    function calculate_area($l, $w)
    {
        echo "the rectangle of lenght $l and width $w has an area of " . ($l * $w);
    }
    if ($_POST['length'] && $_POST['width']) {
        calculate_area($_POST['length'], $_POST['width']);
    }
    ?>
    
    </div>
</body>
</html>
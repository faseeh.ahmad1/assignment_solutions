<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5-4</title>
</head>

<body>
    <?php
    $months = array(
        "January" => 31, "February" => "28 days, if leap year 29", "March" => 31, "April" => 30, "May" => 31,
        "June" => 30, "July" => 31, "August" => 31, "September" => 30, "October" => 31, "November" => 30, "December" => 31
    );

    // function to create single option
    function createOption($value, $selected)
    {
        echo $selected;
        echo "<option name='month'";
        if ($selected)
            echo " selected ";

        echo "value='$value'>$value</option>";
    }
    ?>
    <!-- form to take user input -->
    <form method="POST">
        <label for="month">Please select a month </label>
        <select name="month">
            <?php
            foreach ($months as $month => $value) {
                createOption($month, $month == $_POST['month']);
            }
            ?>
        </select>
        <br />
        <input type="submit" />
    </form>
    <!-- output the result after form submission -->
    <?php
    if ($_POST['month'])
        echo "<p>The month of " . $_POST['month'] . " has " . $months[$_POST['month']] . " days</p>";
    ?>
</body>

</html>
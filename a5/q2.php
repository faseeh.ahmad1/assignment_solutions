<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5-2</title>
</head>
<body>
    <!-- declaring and executing function to calculate area -->
    <?php
        function calculate_area($l,$w){
            echo "the rectangle of lenght $l and width $w has an area of ".($l*$w);
        }

        calculate_area(12,8);
    ?>
</body>
</html>
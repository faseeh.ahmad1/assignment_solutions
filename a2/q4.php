<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-4</title>
    <link rel="stylesheet" href="/assignments/a2/q4.css">

</head>

<body>
    <table>
        <tr>
            <?php
            for ($x = 1; $x <= 7; $x++) {
                echo "<th>$x</th>";
            }
            ?>
        </tr>
        <?php
        for ($x = 2; $x <= 7; $x++) {
            echo '<tr>';
            for ($y = 1; $y <= 7; $y++) {
                echo "<td>" . $y * $x . "</td>";
            }
            echo "</tr>";
        }
        ?>
    </table>
</body>

</html>
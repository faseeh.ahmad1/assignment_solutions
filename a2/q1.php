<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-1</title>
</head>
<body>
    <?php
        $month = date('F',time());
        echo ($month==="August") ? ("It's August, so it's really hot.") : "Not August, so at least not in the peak of the heat.";
    ?>
</body>
</html>
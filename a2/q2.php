<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-2</title>
</head>
<body>
    <?php
    //while loop
        $x = 1;
        while($x<10){
            echo "abc ";
            $x++;
        }

        echo "</br>";
    
    //do while loop
        $x=1;
        do{
            echo "xyz ";
            $x++;
        }
        while($x<10);

        echo "</br>";
    
    //for loop
        for($x=1;$x<10;$x++){
            echo "$x ";
        }

    //2nd for loop
    //using ASCII to print corresponding characters
        for($x=65;$x<=70;$x++){
            echo "</br> Item". chr($x);
        }
    ?>
</body>
</html>
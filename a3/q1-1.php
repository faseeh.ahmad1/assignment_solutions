<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3-1 form</title>
</head>

<body style="display:flex; justify-content:center;align-items:center;min-height:100vh">
        <form action="q1-2.php" method="POST" style="border-radius:10px;padding:50px 20px;background-color:#EEEEEE;display:flex; align-items:center;flex-direction:column;min-width: 40vw;">
            <label for="city" style="align-self:flex-start">FAVOURITE CITY</label>
            <input type="text" id="city" name="city" required style="margin-top:20px;width:100%">
            <input type="submit" style="margin-top:20px; width:50%">
        </form>

</body>

</html>
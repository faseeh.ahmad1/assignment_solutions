<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3-3</title>
</head>

<body style="display:flex; justify-content:center;align-items:center;min-height:100vh">
    <?php
    echo "<div style='display:flex;flex-direction:column'>";
    if ($_POST["day"]) {
        $day = strtolower($_POST["day"]);
        if ($day == 'monday') {
            echo "<h1>Laugh on Monday, laugh for danger. </h1>";
        } else if ($day == 'tuesday') {
            echo "<h1>Laugh on Tuesday, kiss a stranger.</h1>";
        } else if ($day == 'wednesday') {
            echo "<h1>Laugh on Wednesday, laugh for a letter.</h1>";
        } else if ($day == 'thursday') {
            echo "<h1>Laugh on Thursday, something better.</h1>";
        } else if ($day == 'friday') {
            echo "<h1>Laugh on Friday, laugh for sorrow.</h1>";
        } else if ($day == 'saturday') {
            echo "<h1>Laugh on Saturday, joy tomorrow. </h1>";
        } else {
            echo "<h1>Invalid day of week selected</h1>";
        }
        echo "<a href=\"javascript:history.go(-1)\" style='border-radius:5px;text-decoration:none;cursor:pointer;padding:5px;border:1px solid #AAA;max-width:fit-content'>
                GO BACK
            </a></div>";
    } else {
        echo "
                <form method='POST' style='border-radius:10px;padding:50px 20px;background-color:#EEEEEE;display:flex; align-items:center;flex-direction:column;min-width: 40vw;'>
                    <label for='city' style='align-self:flex-start'>Enter Day of week</label>
                    <input type='text' id='day' name='day' required style='margin-top:20px;width:100%'>
                    <input type='submit' style='margin-top:20px; width:50%''>
                </form>
                ";
    }
    ?>



</body>

</html>

//test

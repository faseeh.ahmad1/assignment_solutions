<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5-6</title>
</head>

<body>
    <h1>Assignment 4 - Question 6 </h1>
    <h1>Manipulate Array</h1>
    <?php
    $temperatures = array(
        68, 70, 72, 58, 60, 79, 82, 73, 75, 77, 73, 58, 63, 79, 78,
        68, 72, 73, 80, 79, 68, 72, 75, 77, 73, 78, 82, 85, 89, 83
    );
    //sorting array in ascending order
    sort($temperatures);
    
    //finding average 
    $average = 0;
    foreach ($temperatures as $temp) {
        $average = $average + $temp;
    }
    $average = $average / sizeof($temperatures);

    //printing average temperature
    echo "<h3>Average temperature is :" . number_format((float)$average, 2, '.', ',  ') . "&deg F .</h3>";
    
    //printing last 5 entries as highest
    echo "<h4>Highest 5 temperatures are:</h4>";
    for ($x = sizeof($temperatures) - 5; $x < sizeof($temperatures); $x++) {
        echo "$temperatures[$x], ";
    }

    //printing first 5 entries as lowest
    echo "<h4>Lowest 5 temperatures are:</h4>";
    for ($x = 0; $x < 5; $x++) {
        echo "$temperatures[$x], ";
    }
    ?>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4-2</title>
</head>

<body>
    <h1>Assignment 4 - Question 2 </h1>
    <h1>Simple Array Loop</h1>
    <!-- declaring array,printing and sorting  -->
    <?php
    $cities = array('Tokyo', 'Mexico', 'New York', 'Mumbai', 'Seoul', 'Shanghai', 'Lagos', 'Buenos', 'Cairo', 'London');
    foreach ($cities as $city) {
        echo "$city,";
    }
    sort($cities)
    ?>

    <!-- printing sorted cities in Unordered list -->
    <h3>Sorted list of cities</h3>
    <ul>
        <?php
        foreach ($cities as $city) {
            echo "<li>$city</li>";
        }
        ?>
    </ul>

    <!-- pushing new values and sorting again  -->
    <?php
    array_push($cities, 'Los Angeles', 'Calcutta', 'Osaka', 'Beijing');
    sort($cities);
    ?>

    <!-- again printing sorted cities in Unordered list -->
    <h3>Updated list of cities</h3>
    <ul>
        <?php
        foreach ($cities as $city) {
            echo "<li>$city</li>";
        }
        ?>
    </ul>
</body>

</html>
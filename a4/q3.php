<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4-3</title>
</head>

<body>
    <?php
    ini_set('display_errors','On');
    error_reporting(E_ALL);

    //If form is already submitted
    if (isset($_POST['weather'])) {
        echo "<p style='font-weight:bold'>In " . $_POST['city'] . " in month of " . $_POST['month'] .
            ", " . $_POST['year'] . " You observed following weathers:<br/>";
        foreach ($_POST['weather'] as $w) {
            echo "$w, ";
        }
        echo "</p>";
        echo "<a href=\"javascript:history.go(-1)\" style='border-radius:5px;text-decoration:none;cursor:pointer;padding:5px;border:1px solid #AAA;max-width:fit-content'>
        GO BACK 
        </a>";
    }
    //If form is not subitted before.
    else {
        echo "
    <h1>Assignment 4 - Question 3 </h1>
    <h1>Arrays from User Input</h1>
    <div style='display:flex;justify-content:center'>
        <form method='POST' style='border-radius:10px;padding:50px 20px;background-color:#EEEEEE;display:flex; align-items:center;flex-direction:column;min-width: 40vw;'>
            <label for='city' style='align-self:flex-start'>City</label>
            <input type='text' id='city' name='city' required style='margin-top:20px;width:100%'>
            <label for='month' style='align-self:flex-start;margin-top:20px'>Month</label>
            <input type='text' id='month' name='month' required style='margin-top:20px;width:100%'>
            <label for='year' style='align-self:flex-start;margin-top:20px'>Year</label>
            <input type='text' id='year' name='year' required style='margin-top:20px;width:100%'>
            <label for='weather' style='align-self:flex-start;margin-top:20px'>Select Weather Conditions</label>
            ";

        $weathers = array('rain', 'sunshine', 'clouds', 'hail', 'sleet', 'snow', 'wind');
        foreach ($weathers as $weather) {
            echo "
                    <div style='display:flex;align-self:flex-start'>
                    <input type='checkbox' name='weather[]' value='$weather'>
                    <label for='$weather'>$weather</label><br>
                    </div>
                    ";
        }
        echo
        "<input type='submit'> 
        </form>
    </div>";
    }
    ?>


</body>

</html>
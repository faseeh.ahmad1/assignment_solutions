<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4-4</title>
</head>

<body>
    <?php
    //storing new added values in session to persist
    session_start();
    ?>
    <h1>Assignment 4 - Question 4 </h1>
    <h1>Add User Input to array</h1>

    <h2>
        Travel takes many forms,whether across town, across the country, or around
        the world.<br />Here is a list of some common modes of transportation.
    </h2>
    <ul>
        <?php
        //reset action
        if ($_POST['reset']) 
            $_SESSION["modes"] = NULL;
        
        //if page is loaded first time or modes are null 
        if (!$_SESSION["modes"])
            $_SESSION["modes"] = array('Automobile', 'Jet', 'Ferry', 'Subway');
        

        //when user enters new values append in previous array
        if ($_POST['new']) {
            $new_modes = explode(",", $_POST['new']);
            $_SESSION["modes"] = array_merge($_SESSION["modes"], $new_modes);
        }

        //printing values in unordered list
        foreach ($_SESSION["modes"] as $mode) 
            echo "<li>" . $mode . "</li>";
        
        ?>
    </ul>

    <!-- form to take new values and call reset -->
    <form method="POST">
        <input type="text" name="new" placeholder="add more items(csv)">
        <input type="submit">
        <input type="submit" name="reset" value="reset" />

    </form>

</body>

</html>
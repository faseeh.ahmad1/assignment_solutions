<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4-5</title>
</head>

<body>
    <h1>Assignment 4 - Question 5 </h1>
    <h1>Associative Array</h1>
    <?php
    //associative array of cities and countries
    $city = array(
        "Japan" => "Tokyo", "Mexico" => "Mexico City", "USA" => "NY", "India" => "Mumbai",
        "Korea" => "Seoul", "China" => "Shanghai", "Nigeria" => "Lagos", "Argentina" => "Buenos Aires",
        "Egypt" => "Cairo", "England" => "London"
    );

    //If city is not already selected
    if (!$_POST['city']) {
        echo "<label for='cars'>Choose a city:</label>
        <form method='POST'>
            <select name='city'>";
        foreach ($city as $x => $x_value) {
            echo "<option value='$x_value+$x'>$x_value</option>";
        }
        echo "</select>
            <input type='submit'>
        </form>";
    }
    
    //If city is selected then display
    else{
        $name  = explode('+',$_POST['city']);
        echo "$name[0] is in $name[1]";
        echo "<a href=\"javascript:history.go(-1)\" style='border-radius:2px;text-decoration:none;cursor:pointer;padding:5px;border:1px solid #AAA;max-width:fit-content;margin-left:10px'>
        GO BACK 
        </a>";
    }
    ?>
</body>

</html>
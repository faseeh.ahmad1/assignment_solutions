<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4-7</title>
</head>

<style type="text/css">
    td, th {width: 8em; border: 1px solid black; padding-left: 4px;}
    th {text-align:center;}
    table {border-collapse: collapse; border: 1px solid black;}
</style> 

<body>
    <h1>Assignment 4 - Question 7 </h1>
    <h1>Multi-Dimensional Array</h1>
    <?php
        $multiCity = array (
            array("City","Country","Continent"),
            array("Tokyo"," Japan"," Asia"),
            array("Mexico City"," Mexico"," North America"),
            array("New York City"," USA"," North America"),
            array("Mumbai","India"," Asia"),
            array("Seoul", "Korea", "Asia"),
            array("Shanghai"," China"," Asia"),
            array("Lagos", "Nigeria", "Africa"),
            array("Buenos Aires","Argentina", "South America"),
            array("Cairo", "Egypt","Africa"),
            array("London"," UK","Europe"),
          );

    ?>
    <table>
        <tr>
            <!-- printing multiDimentional array -->
            <?php
                echo "<th>".$multiCity[0][0]."</th>";
                echo "<th>".$multiCity[0][1]."</th>";
                echo "<th>".$multiCity[0][2]."</th>";
                for($x=1;$x<sizeof($multiCity);$x++){
                    echo "<tr>";
                    //using foreach loop for subarrays
                    foreach($multiCity[$x] as $y){
                        echo "<td>$y</td>";
                    }
                    echo "</tr>";
                }
            ?>
        </tr>
    </table>
</body>
</html>